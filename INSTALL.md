Installation de l'Api Rest
===========================

Prérequis :

- PHP 5.6 ou 7
- Un utilisateur Mysql
- Une base de données MySQL


Installer symfony et ces dépendances (Composer) :

    composer install

Création des tables dans la base de donnnés :

    php bin/console doctrine:schema:update --force

Insertion d'un jeu de données :

    php bin/console doctrine:fixtures:load
    
Mise en production des assets de l'application :

    php bin/console assets:install --symlink