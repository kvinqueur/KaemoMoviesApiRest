<?php

namespace tests\KaemoBundleAppBundle\Util;

use Kaemo\Bundle\AppBundle\Entity\Video;

class VideoTest extends \PHPUnit_Framework_TestCase
{
    public function testSetTitle() {
        $video = new Video();

        $video->setTitle("Vidéoconférence");

        $this->assertEquals("Vidéoconférence", $video->getTitle());
    }

    public function testSetRealisateur() {
        $video = new Video();

        $video->setRealisator("John Doe");

        $this->assertEquals("John Doe", $video->getRealisator());
    }
}