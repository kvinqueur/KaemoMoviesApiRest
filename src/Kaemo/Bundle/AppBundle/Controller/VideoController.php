<?php

namespace Kaemo\Bundle\AppBundle\Controller;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Kaemo\Bundle\AppBundle\Entity\Video;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

class VideoController extends Controller
{
    /**
     * @ApiDoc(
     *     description="Displays the video list",
     *     output= { "class"=Video::class, "collection"=true }
     * )
     *
     * @Rest\View()
     * @Rest\Get("/videos")
     * @Rest\QueryParam(name="from", requirements="[0-9]{8}", default="", description="Start date")
     * @Rest\QueryParam(name="to", requirements="[0-9]{8}", default="", description="End date")
     * @Rest\QueryParam(name="realisator", requirements="[a-zA-Z]+", default="", description="Realisator")
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @return View
     */
    public function getVideosAction(Request $request, ParamFetcher $paramFetcher)
    {
        $from = $paramFetcher->get('from');
        $to = $paramFetcher->get('to');
        $realisator = $paramFetcher->get('realisator');

        $filter = $this->container->get('kaemo_bundle_app.filter');

        $videos = $filter->filter(null, $from, $to, $realisator);

        if (empty($videos)) {
            return $this->videoNotFound();
        }

        return $videos;
    }


    /**
     * @ApiDoc(
     *     description="Display a video by id"
     * )
     *
     * @Rest\View()
     * @Rest\Get("/videos/{id}")
     * @param Request $request
     * @return View
     */
    public function getVideoAction(Request $request)
    {
        $id = $request->get('id');
        $filter = $this->container->get('kaemo_bundle_app.filter');

        $video = $filter->filter($id, null, null, null);

        if (empty($video)) {
            return $this->videoNotFound();
        }

        return $video;
    }

    /**
     * @return View
     */
    public function videoNotFound()
    {
        return View::create(['message' => 'Video(s) not found'], Response::HTTP_NOT_FOUND);
    }
}
