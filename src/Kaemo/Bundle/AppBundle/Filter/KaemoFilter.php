<?php
/**
 * Created by PhpStorm.
 * User: kvinqueur
 * Date: 21/10/16
 * Time: 15:37
 */

namespace Kaemo\Bundle\AppBundle\Filter;


use Doctrine\ORM\EntityManagerInterface;

class KaemoFilter
{

    /**
     * KaemoUrl constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return EntityManagerInterface
     */
    public function filter($id, $from, $to, $realisator)
    {
        $videoRepository = $this->em->getRepository("KaemoBundleAppBundle:Video");

        if ($from != null && $to != null) {
            $videos = $videoRepository->findByDate($from, $to);
        } elseif ($realisator != null) {
            $videos = $videoRepository->findByRealisator($realisator);
        } elseif ($id != null) {
            $videos = $videoRepository->find($id);
        } else {
            $videos = $videoRepository->findAll();
        }

        return $videos;
    }
}