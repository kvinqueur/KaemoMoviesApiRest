<?php
/**
 * Created by PhpStorm.
 * User: kvinqueur
 * Date: 20/10/16
 * Time: 18:21
 */

namespace Kaemo\Bundle\AppBundle\DataFixtures;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Kaemo\Bundle\AppBundle\Entity\Video;

class LoadVideoData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $video = new Video();

        $video->setTitle("Introduction aux méthodes agiles");
        $video->setRealisator("Monsieur Dupond");

        $video2 = new Video();

        $video2->setTitle("Capifony - Déploiement d'une application Symfony");
        $video2->setRealisator("Monsieur Dupond");

        $manager->persist($video);
        $manager->persist($video2);

        $manager->flush();
    }
}