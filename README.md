Test - Développement PHP
=========

### Description du test

Ce test a pour but de mettre en œuvre une application permettant 2 choses :
1. Permettre à l’aide d’une ligne de commande le stockage de fiches de film (champs à
stocker : Titre du film, Date d’ajout, Réalisateur)
2. Permettre la récupération de fiches précédemment crées à l’aide d’une **API REST au
format JSON**

-----------------

### Éléments requis

* Vous devez utiliser un Framework PHP ( Exemple : Symfony ou Laravel )
* Vous avez le choix dans la méthode ou le procédé de stockage des informations
(exemple : MySQL)
* Vous devez tester unitairement votre code (PHPUnit)
* Vous devez utiliser Composer pour gérer vos dépendances

-----------------

### Description de l'API

**GET /api/videos**

Sortie souhaitée :
```json
    {
        "videos": [
            {
                "id": 1,
                "title" : "The wire",
                "date" : "2015-01-01 00:00:00",
                "realisator" : "David Simon"
            }
        ],
        "count" : 1
    }
```

Paramètres :
* **from** (optionnel) Date de début
* **to** (optionnel) Date de fin
* **realisator** (optionnel) Auteur

Utilisation :
* /api/videos
* /api/videos?from=20150101&to=20151231
* /api/videos?realisator=David

**GET /api/videos/\<id>**

Sortie souhaitée :
```json
    {
        "video": {
                "id": 1,
                "title" : "The wire",
                "date" : "2015-01-01 00:00:00",
                "realisator" : "David Simon"
        }
    }
```